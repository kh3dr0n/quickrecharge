package com.kh3dr0n.quickrecharge;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.CameraHostProvider;
import com.commonsware.cwac.camera.SimpleCameraHost;


public class MainActivity extends Activity implements CameraHostProvider,OnGestureListener {
	 private camreFragment current=null;
	 final String TAG = "QuickRecharge";
	 public int width = 0;
	 public int height = 0;
	 private GestureDetector gDetector;
	 ImageView help = null;
    @SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);

    	
    	
    	String lang = "eng";
    	String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/QuickRecharge/";
        
        setContentView(R.layout.activity_main);
        gDetector = new GestureDetector(this);
        
		File dir = new File(Environment.getExternalStorageDirectory().toString() + "/QuickRecharge/");
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				Log.v(TAG, "ERROR: Creation of directory  QuickRecharge on sdcard failed");
				return;
			} else {
				Log.v(TAG, "Created directory QuickRecharge on sdcard");
			}
		}
		
		
		dir = new File(Environment.getExternalStorageDirectory().toString() + "/QuickRecharge/tessdata/");
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				Log.v(TAG, "ERROR: Creation of directory  QuickRecharge on sdcard failed");
				return;
			} else {
				Log.v(TAG, "Created directory QuickRecharge on sdcard");
			}
		}
		
		
		if (!(new File(DATA_PATH + "tessdata/" + lang + ".traineddata")).exists()) {
			try {

				AssetManager assetManager = getAssets();
				InputStream in = assetManager.open("tessdata/" + lang + ".traineddata");
				//GZIPInputStream gin = new GZIPInputStream(in);
				OutputStream out = new FileOutputStream(DATA_PATH
						+ "tessdata/" + lang + ".traineddata");

				// Transfer bytes from in to out
				byte[] buf = new byte[1024];
				int len;
				//while ((lenf = gin.read(buff)) > 0) {
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				//gin.close();
				out.close();
				
				Log.v(TAG, "Copied " + lang + " traineddata");
			} catch (IOException e) {
				Log.e(TAG, "Was unable to copy " + lang + " traineddata " + e.toString());
			}
		}
		
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
        
        
        current=new camreFragment();

        getFragmentManager().beginTransaction().replace(R.id.container, current).commit();
    }
    
    @Override
    protected void onStart() {
    	// TODO Auto-generated method stub
    	super.onStart();
    	help = (ImageView) findViewById(R.id.help);
    	boolean firstrun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("firstrun", true);
    	if(!firstrun){
    		help.setVisibility(View.INVISIBLE);
        }else{
    		getSharedPreferences("PREFERENCE", MODE_PRIVATE)
            .edit()
            .putBoolean("firstrun", false)
            .commit();
        }
    	
    	
    }
   
    
    @Override
    public boolean onTouchEvent(MotionEvent me) {
    return gDetector.onTouchEvent(me);
    }
    @Override
    public CameraHost getCameraHost() {
      return(new SimpleCameraHost(this));
    }
	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
	
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		if(help.getVisibility() == View.VISIBLE){
			help.setVisibility(View.INVISIBLE);
		}else
		current.makeshot();
		return false;
	}
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}
	@Override
	public void onLongPress(MotionEvent e) {
		if(help.getVisibility() == View.VISIBLE){
			help.setVisibility(View.INVISIBLE);
		}else	
		current.takefocus();
		
	}
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}
}

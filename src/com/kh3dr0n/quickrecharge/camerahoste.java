package com.kh3dr0n.quickrecharge;

import java.io.ByteArrayOutputStream;
import java.io.File;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.net.Uri;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;
import com.googlecode.tesseract.android.TessBaseAPI;

public class camerahoste extends SimpleCameraHost implements Camera.FaceDetectionListener{
	Activity context = null;
	int height = 0;
	int width = 0;
	ProgressDialog p;
	Boolean isAutoFocus = false;
	public camerahoste(Activity _ctxt,int x,int y,ProgressDialog pg,Boolean focus) {
		super(_ctxt);
		width = x;
		height = y;	
		p = pg;
		context = _ctxt;
		isAutoFocus = focus;
		// TODO Auto-generated constructor stub
	}
	
	public camerahoste(Context _ctxt) {
		super(_ctxt);
		// TODO Auto-generated constructor stub
	}
	@Override
	public boolean useSingleShotMode() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	protected File getPhotoDirectory() {
		// TODO Auto-generated method stub
		return new File(Environment.getExternalStorageDirectory().toString() + "/QuickRecharge");
	}
	@Override
	protected String getPhotoFilename() {
		// TODO Auto-generated method stub
		return "/test.jpg";
	}
	@Override
	protected File getPhotoPath() {
		// TODO Auto-generated method stub
		return new File(Environment.getExternalStorageDirectory().toString() + "/QuickRecharge/test.jpg");
	}
	@Override
	protected boolean scanSavedImage() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void onFaceDetection(Face[] faces, Camera camera) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	@SuppressWarnings("static-access")
	@Override
	public void saveImage(PictureTransaction xact, byte[] image) {
		
		// TODO Auto-generated method stub
		Log.v("QuickRecharge","bits");
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		//Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
		Bitmap bitmap = BitmapFactory.decodeByteArray(image,0 ,image.length);
		
		double scale = (width/500.) * (bitmap.getHeight()/(double)height);
		Log.v("QuickRecharge",String.valueOf(scale));
		bitmap = bitmap.createBitmap(bitmap,(int)(bitmap.getWidth() - 432*scale)/2,(int)(bitmap.getHeight() - 64*scale)/2,(int)(432*scale),(int)(64*scale));
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		
		super.saveImage(xact, byteArray);

		
		BitmapFactory.Options options2 = new BitmapFactory.Options();
		options2.inSampleSize = 4;
		Bitmap bitmapfile = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().toString() + "/QuickRecharge/test.jpg", options2);

		
		Log.v("QuickRecharge", "Before baseApi");

		TessBaseAPI baseApi = new TessBaseAPI();
		baseApi.setDebug(true);
		baseApi.init(Environment.getExternalStorageDirectory().toString() + "/QuickRecharge/","eng");
		baseApi.setVariable("tessedit_char_whitelist", "0123456789");
		baseApi.setImage(bitmapfile);
		String recognizedText = baseApi.getUTF8Text();
		baseApi.end();
		Log.v("QuickRecharge", "OCRED TEXT: " + recognizedText.trim());
		
	
		TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String carrierName = manager.getNetworkOperatorName();
		
		
		
		Log.v("QuickRecharge", carrierName);
		context.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				if(p.isShowing())
					p.dismiss();
				
			}
		});
		
		switch (carrierName) {
		case "Ooredoo TN":
			try {
				String uri = "tel:*101*"+recognizedText.trim()+ "%23";
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));
				context.startActivity(callIntent);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case "ORANGE TN":
			try {
				String uri = "tel:*100*"+recognizedText.trim()+ "%23";
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));
				context.startActivity(callIntent);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;
		case "TUNTEL":
			try {
				String uri = "tel:*123*"+recognizedText.trim()+ "%23";
				Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));
				context.startActivity(callIntent);
			}catch(Exception e) {
				e.printStackTrace();
			}
			break;

		default:
			break;
		}
		
	}

	
    @Override
    @TargetApi(16)
    public void onAutoFocus(boolean success, Camera camera) {
      super.onAutoFocus(success, camera);
     isAutoFocus = false;
    }
    
}


    
    
    


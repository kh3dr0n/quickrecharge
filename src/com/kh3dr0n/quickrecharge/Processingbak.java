package com.kh3dr0n.quickrecharge;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

public class Processingbak extends AsyncTask<Integer, Void, Void>{

	private ProgressDialog dialog;
	private Context mContext;
	public Processingbak(Context context) {
		// TODO Auto-generated constructor stub
		mContext = context;


	}

	 @Override
	    protected void onPreExecute() {
	        dialog.setMessage("Doing something, please wait.");
	        dialog.show();
	    }
	
	@Override
	protected Void doInBackground(Integer... params) {
		// TODO Auto-generated method stub
		
		
		
		
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 4;
		Bitmap bitmapfile = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().toString() + "/QuickRecharge/test.jpg", options);

		
		Log.v("QuickRecharge", "Before baseApi");

		TessBaseAPI baseApi = new TessBaseAPI();
		baseApi.setDebug(true);
		baseApi.init(Environment.getExternalStorageDirectory().toString() + "/QuickRecharge/","eng");
		baseApi.setVariable("tessedit_char_whitelist", "0123456789");
		baseApi.setImage(bitmapfile);
		String recognizedText = baseApi.getUTF8Text();
		baseApi.end();
		Log.v("QuickRecharge", "OCRED TEXT: " + recognizedText);
		
	
		TelephonyManager manager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
		String carrierName = manager.getNetworkOperatorName();
		
		
		
		return null;
	}
	
	 @Override
	    protected void onPostExecute(Void result) {
	        if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
	    }

}

package com.kh3dr0n.quickrecharge;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.commonsware.cwac.camera.CameraFragment;
import com.commonsware.cwac.camera.CameraView;
import com.commonsware.cwac.camera.PictureTransaction;
import com.commonsware.cwac.camera.SimpleCameraHost;




public class camreFragment  extends CameraFragment{
		int click = 1;
		ProgressDialog pd;
		Boolean isAutofocus = false;


		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			
			

			DisplayMetrics displaymetrics = new DisplayMetrics();
			getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			int height = displaymetrics.heightPixels;
			int width = displaymetrics.widthPixels;
			pd = new ProgressDialog(getActivity());
			

			  SimpleCameraHost.Builder builder= new SimpleCameraHost.Builder(new camerahoste(getActivity(),width,height,pd,isAutofocus));
			  setHost(builder.useFullBleedPreview(true).build());

		}
	  @SuppressLint("ClickableViewAccessibility") @Override
	  public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
		  
	    View content=inflater.inflate(R.layout.camera, container, false);
	    CameraView cameraView=(CameraView)content.findViewById(R.id.camera);
	    
	    setCameraView(cameraView);
	    lockToPortrait(true);
	    
//		final GestureDetector detector = new GestureDetector(getActivity(),new GestureDetector.SimpleOnGestureListener(){
//			Context context = getActivity().getApplicationContext();
//			public boolean onSingleTapConfirmed(MotionEvent e) {
//				// TODO Auto-generated method stub
//				Toast.makeText(context,"onSingleTapConfirmed",Toast.LENGTH_LONG).show();
//				return super.onSingleTapConfirmed(e);
//			}
//			
//			@Override
//			public boolean onSingleTapUp(MotionEvent e) {
//				// TODO Auto-generated method stub
//				
//				  Toast.makeText(context,"TapUp",Toast.LENGTH_LONG).show();
//				  PictureTransaction xact=new PictureTransaction(getHost());
//					  if(!isAutofocus){
//					  try {
//						  getActivity().runOnUiThread(new Runnable() {
//								
//								@Override
//								public void run() {
//									pd.setMessage("Working");
//									pd.show();
//									
//								}
//							});
//						  takePicture(xact);
//						  
//						  click = 1;
//					} catch (Exception ex) {
//						
//					} }
//				return super.onSingleTapConfirmed(e);
//				
//			}



//			@Override
//			public void onLongPress(MotionEvent e) {
//				Toast.makeText(context,"LongPress",Toast.LENGTH_LONG).show();
//				isAutofocus = true;
//				autoFocus();
//				
//			}
//		});
		
	    ImageView iv = (ImageView) content.findViewById(R.id.imageView1);
//	    iv.setOnTouchListener(new View.OnTouchListener() {
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				Toast.makeText(getActivity().getApplicationContext(),event.toString(),Toast.LENGTH_SHORT).show();
//				return detector.onTouchEvent(event);
//			}
//		});

	    return(content);
	  }
	  
	  public void makeshot(){
		  PictureTransaction xact=new PictureTransaction(getHost());
//		  if(!isAutofocus){
		  try {
			  getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						pd.setMessage("Working");
						pd.show();
						
					}
				});
			  takePicture(xact);
			  
			  click = 1;
		} catch (Exception ex) {
			
		}
//		  }
	  }
	  public void takefocus(){
//			isAutofocus = true;
			autoFocus();
	  }
	  
}
